variable "ami" {
  type = string
  description = "Ubuntu AMI ID in eu-central-1 region"
  default = "ami-065deacbcaac64cf2"
}

variable "instance_type" {
  type = string
  description = "EC2 instance type"
  default = "t2.micro"
}

variable "name_tag" {
  type = string
  description = "Name tag for the EC2 instance"
  default = "My EC2 instance"
}