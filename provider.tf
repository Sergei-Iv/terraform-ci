terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.18.0"
    }
  }

  backend "http" {
    address = "https://gitlab.com/api/v4/projects/57609402/terraform/state/$TF_STATE_NAME"
    lock_address = "https://gitlab.com/api/v4/projects/57609402/terraform/state/$TF_STATE_NAME/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/57609402/terraform/state/$TF_STATE_NAME/lock"
  }
}

provider "aws" {
  region = "eu-central-1"
}